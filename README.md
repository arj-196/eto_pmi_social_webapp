Docker Commands
---------------
    # will only work with tozd/meteor-mongodb 
    
    docker run -d \
            -e "ROOT_URL=http://localhost:3000" \
            -e "MONGO_URL=mongodb://192.168.99.100:27017/meteor" \
            -e "PORT=80" \
            -e "MONGO_OPLOG_URL=mongodb://192.168.99.100:27017/local" \
            -p 80:80 \
            test/pmi
            
    docker run -d \
                -e "ROOT_URL=http://localhost:3000" \
                -e "MONGO_URL=mongodb://192.168.99.100:27017/meteor" \
                -e "PORT=80" \
                -e "MONGO_OPLOG_URL=mongodb://192.168.99.100:27017/local" \
                -p 80:80 \
                publiciseto/pmi_webapp

    docker run -it \
                -e "ROOT_URL=http://localhost:3000" \
                -e "MONGO_URL=mongodb://mongo.pmi.marathon.commonsense:27017/meteor" \
                -e "PORT=80" \
                -e "MONGO_OPLOG_URL=mongodb://mongo.pmi.marathon.commonsense:27017/local" \
                -p 80:80 \
                publiciseto/pmireport


    docker run -it \
                -e "ROOT_URL=http://localhost:3000" \
                -e "MONGO_URL=mongodb://10.91.89.71:27017/meteor" \
                -e "PORT=80" \
                -e "MONGO_OPLOG_URL=mongodb://10.91.89.71:27017/local" \
                -p 80:80 \
                publiciseto/pmireport

    docker run -it \
                -e "ROOT_URL=http://localhost:3000" \
                -e "MONGO_URL=mongodb://192.168.99.100:27017/meteor" \
                -e "PORT=80" \
                -e "MONGO_OPLOG_URL=mongodb://192.168.99.100:27017/local" \
                -p 80:80 \
                publiciseto/pmireport