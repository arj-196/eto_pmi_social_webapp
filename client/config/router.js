Router.configure({
    layoutTemplate: 'mainLayout',
    notFoundTemplate: 'notFound'

})

Router.route('/', function () {
    this.render('login')
    this.layout('blankLayout')
})

Router.route('/logout', function () {
    Router.go('/')
})

Router.route('/home', function () {
    this.render('home')
})


Router.route('/dashboard/:country', {
    name: 'dashboard.country',
    template: 'OnePageDashboard',
})


/**
 * Deprecated
 */

Router.route('/executive/:country', {
    name: 'executive.country',
    template: 'ExecutiveSummaryDashboard',
})

Router.route('/kpis/:country', {
    name: 'kpi.country',
    template: 'KPIDashboard',
})

Router.route('/influencers/:country', {
    name: 'influencer.country',
    template: 'InfluDashboard',
})

Router.route('/authors/:country', {
    name: 'author.country',
    template: 'AuthorDashboard',
})

Router.route('/media/:country', {
    name: 'media.country',
    template: 'MediaDashboard',
})


Router.route('/socialmedia/:country', {
    name: 'socialmedia.country',
    template: 'SocialMediaDashboard',
})

