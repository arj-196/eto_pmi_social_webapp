Template.navigation.rendered = function () {

    // Initialize metisMenu
    $('#side-menu').metisMenu();

};

Template.navigation.helpers({
    Countries: function () {
        return [
            {name: "France", value: "france"},
            {name: "Germany", value: "germany"},
            {name: "Italy", value: "italy"},
            {name: "UK", value: "uk"},
            {name: "US", value: "us"},
            {name: "Spain", value: "spain"},
            // {name: "Morocco", value: "morocco"},
            // {name: "Egypt", value: "egypt"},
            // {name: "Jordan", value: "jordan"},
            // {name: "Ukraine", value: "ukraine"},
            // {name: "GCC", value: "gcc"},
            // {name: "Israel", value: "israel"},
            // {name: "Russia", value: "russia"},
            // {name: "Kazakhstan", value: "kazakhstan"},
            // {name: "Turkey", value: "turkey"},
            // {name: "South Africa", value: "southafrica"},
            // {name: "Nordics", value: "nordics"},
            // {name: "Switzerland", value: "switzerland"},
            // {name: "Slovenia", value: "slovenia"},
            // {name: "Poland", value: "poland"},
            // {name: "Portugal", value: "portugal"},
            // {name: "Holland", value: "holland"},
            // {name: "Romania", value: "romania"},
            // {name: "Japan", value: "japan"},
            // {name: "India", value: "india"},
            // {name: "Singapore", value: "singapore"},
            // {name: "Malaysia", value: "malaysia"},
            // {name: "Hong Kong", value: "hongkong"},
            // {name: "Korea", value: "korea"},
            // {name: "Dominican Republic", value: "dominicanrepublic"},
            // {name: "Colombia", value: "colombia"},
            // {name: "Senegal", value: "senegal"},
        ].sort(function (a, b) {
            if (a.value < b.value) return 1;
            if (b.value < a.value) return -1;
            return 0;
        }).reverse()
    }
})

Template.navigation.events({
    'keyup .search-country': function (event) {
        var $this = $(event.target)
        var $parent = $this.parent().parent()
        var items = _.map($parent.find('.item-country'), (d) => {
            return {c: d, v: $(d).data().value}
        })

        var input = $this.val().trim().toLowerCase()
        var matched = _.filter(
            _.map(items, (d) => {
                d.match = d.v.match(input)
                return d
            }),
            (d) => {
                return !_.isNull(d.match)
            }
        )

        $parent.find('.item-country').addClass('hidden')
        matched.forEach(function (d) {
            $(d.c).removeClass('hidden')
        })
    }
})