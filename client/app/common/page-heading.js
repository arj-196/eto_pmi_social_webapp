Template.pageHeading.helpers({

    // Route for Home link in breadcrumbs
    home: 'home',

    ifExistsCountries: function () {
        var params = Iron.controller().getParams()
        return !_.isUndefined(params.country)
    },
    countries: function () {
        // TODO format country name
        return Iron.controller().getParams().country.split(',')
    }
});
