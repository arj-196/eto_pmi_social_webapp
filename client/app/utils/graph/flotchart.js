export function drawFlotChart(floatChartContainer, data, xaxisLabel) {
    // FLOTCHART 1
    data = _.map(data, (d) => {
        return [d.x.getTime(), d.y]
    })

    floatChartContainer.length &&
    $.plot(floatChartContainer,
        [
            {label: xaxisLabel, data: data},
        ]
        ,
        {
            series: {
                lines: {
                    show: false,
                    fill: true
                },
                splines: {
                    show: true,
                    tension: 0.4,
                    lineWidth: 1,
                    fill: 0.4
                },
                points: {
                    radius: 0,
                    show: true
                },
                shadowSize: 2
            },
            grid: {
                hoverable: true,
                clickable: true,
                tickColor: "#d5d5d5",
                borderWidth: 1,
                color: '#d5d5d5'
            },
            colors: ["#1ab394", "#1C84C6"],
            xaxis: {
                mode: 'time'
            },
            yaxis: {
                ticks: 4
            },
            tooltip: true,
            legend: {
                show: true,
                backgroundOpacity: 0.5,
                backgroundColor: "#0e190e",
                margin: [2, 2]
            },
        }
    )
}
