export function drawStackedBarChart(containerId, data) {
    // Stocked bar chart
    new Chartist.Bar(containerId, {
        labels: data.labels.slice(0, 20),
        series: [
            data.series[0].slice(0, 20),
            data.series[1].slice(0, 20),
            data.series[2].slice(0, 20),
        ]
    }, {
        stackBars: true,
        axisY: {
            high: 100,
            labelInterpolationFnc: function(value) {
                // return (value / 1000) + 'k';
                return value;
            }
        }
    }).on('draw', function(data) {
        if(data.type === 'bar') {
            data.element.attr({
                style: 'stroke-width: 30px'
            });
        }
    })
}
