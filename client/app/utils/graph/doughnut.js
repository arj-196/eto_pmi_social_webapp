var faker = require('faker')

var colorScale = d3.scale.category20()

export function drawDoughnutChart(containerId, data) {
    data = _.map(data, (d) => {
        // TODO use d3 for color and highlight
        d.color = colorScale(d.label)
        d.highlight = colorScale(d.label)
        return d
    })
    //DOUGHNUT CHART
    // Options, data for doughnut chart
    var doughnutOptions = {
        segmentShowStroke: true,
        segmentStrokeColor: "#fff",
        segmentStrokeWidth: 2,
        percentageInnerCutout: 45, // This is 0 for Pie charts
        animationSteps: 100,
        animationEasing: "easeOutBounce",
        animateRotate: true,
        animateScale: false
    }
    new Chart(document.getElementById(containerId).getContext("2d")).Doughnut(data, doughnutOptions)

}