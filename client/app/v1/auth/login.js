Template.login.events({
    'submit .login-form': function (event) {
        event.preventDefault()
        var data = $(event.target).serializeArray()
        var email = _.find(data, (d) => {
            return d.name == "email"
        }).value
        var password = _.find(data, (d) => {
            return d.name == "password"
        }).value


        var registeredEmails = [
            'test@test.com',
            'pmireport@publicis-eto.fr'
        ]

        var emailIndex = registeredEmails.indexOf(email)
        if(emailIndex != -1) {
            Router.go('/home')
        } else {
            alert('Wrong Email or Password.')
        }

        // console.log(email, password)
        // Meteor.loginWithPassword(email, password, function (err) {
        //     if(!err){
        //         Router.go('/home')
        //     } else {
        //         Router.go('/')
        //     }
        // })
    }
})