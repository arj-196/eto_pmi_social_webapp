var faker = require('faker')
var cache = new ReactiveDict('cache_social_media.v1')

Template.SocialMediaDashboard.rendered = function () {
    cache.set('feedPosts', null)
    setTimeout(() => {
        drawPieChart({mode: "all"})
    }, 2000)

    setTimeout(() => {
        drawWordCloud({})
    }, 2000)
    drawFlotChart2()
}

Template.SocialMediaDashboard.helpers({
    feeds: () => {

        return [
            {name: "Twitter", value: "Twitter"},
            // {name: "Facebook", value: "facebook"},
            // {name: "Linkedin", value: "linkedin"}
        ]
    },

    ifExistsPosts: () => {
        return !_.isNull(cache.get('feedPosts'))
    },

    feedPosts: () => {
        var posts = cache.get('feedPosts')
        return posts
    }
})

function drawWordCloud(searchQuery) {
    var rawList = StatsSocialWordCloud.find(searchQuery, {sort: {"value.count": -1}, limit: 300}).fetch()

    // formatting word list
    var wordList = _.map(rawList, (d) => {
        return [
            d._id,
            d.value.count
        ]
    })

    // normalising word list
    var min = _.min(wordList, (d) => {
        return d[1]
    })[1]
    var max = _.max(wordList, (d) => {
        return d[1]
    })[1]

    var normalizedList = _.map(wordList, (d) => {
        var value = d[1]
        value = ((value - min) / (max - min)) * 100
        if (value < 20)
            value = 20

        d[1] = value
        return d
    })

    WordCloud(
        document.getElementById('wordcloud-1'),
        {
            list: normalizedList,
        }
    )
}


function drawFlotChart2() {
    // FLOTCHART 2
    // Options, data for charts
    var data2 = [
        [gd(2012, 1, 1), 7], [gd(2012, 1, 2), 6], [gd(2012, 1, 3), 4], [gd(2012, 1, 4), 8],
        [gd(2012, 1, 5), 9], [gd(2012, 1, 6), 7], [gd(2012, 1, 7), 5], [gd(2012, 1, 8), 4],
        [gd(2012, 1, 9), 7], [gd(2012, 1, 10), 8], [gd(2012, 1, 11), 9], [gd(2012, 1, 12), 6],
        [gd(2012, 1, 13), 4], [gd(2012, 1, 14), 5], [gd(2012, 1, 15), 11], [gd(2012, 1, 16), 8],
        [gd(2012, 1, 17), 8], [gd(2012, 1, 18), 11], [gd(2012, 1, 19), 11], [gd(2012, 1, 20), 6],
        [gd(2012, 1, 21), 6], [gd(2012, 1, 22), 8], [gd(2012, 1, 23), 11], [gd(2012, 1, 24), 13],
        [gd(2012, 1, 25), 7], [gd(2012, 1, 26), 9], [gd(2012, 1, 27), 9], [gd(2012, 1, 28), 8],
        [gd(2012, 1, 29), 5], [gd(2012, 1, 30), 8], [gd(2012, 1, 31), 25]
    ]

    var data3 = [
        [gd(2012, 1, 1), 800], [gd(2012, 1, 2), 500], [gd(2012, 1, 3), 600], [gd(2012, 1, 4), 700],
        [gd(2012, 1, 5), 500], [gd(2012, 1, 6), 456], [gd(2012, 1, 7), 800], [gd(2012, 1, 8), 589],
        [gd(2012, 1, 9), 467], [gd(2012, 1, 10), 876], [gd(2012, 1, 11), 689], [gd(2012, 1, 12), 700],
        [gd(2012, 1, 13), 500], [gd(2012, 1, 14), 600], [gd(2012, 1, 15), 700], [gd(2012, 1, 16), 786],
        [gd(2012, 1, 17), 345], [gd(2012, 1, 18), 888], [gd(2012, 1, 19), 888], [gd(2012, 1, 20), 888],
        [gd(2012, 1, 21), 987], [gd(2012, 1, 22), 444], [gd(2012, 1, 23), 999], [gd(2012, 1, 24), 567],
        [gd(2012, 1, 25), 786], [gd(2012, 1, 26), 666], [gd(2012, 1, 27), 888], [gd(2012, 1, 28), 900],
        [gd(2012, 1, 29), 178], [gd(2012, 1, 30), 555], [gd(2012, 1, 31), 993]
    ]

    var dataset = [
        {
            label: "Actual Figures",
            data: data3,
            color: "#1ab394",
            bars: {
                show: true,
                align: "center",
                barWidth: 24 * 60 * 60 * 600,
                lineWidth: 0
            }

        }, {
            label: "Average",
            data: data2,
            yaxis: 2,
            color: "#1C84C6",
            lines: {
                lineWidth: 1,
                show: true,
                fill: true,
                fillColor: {
                    colors: [{
                        opacity: 0.2
                    }, {
                        opacity: 0.2
                    }]
                }
            },
            splines: {
                show: false,
                tension: 0.6,
                lineWidth: 1,
                fill: 0.1
            }
        }
    ]


    var options = {
        xaxis: {
            mode: "time",
            tickSize: [3, "day"],
            tickLength: 0,
            axisLabel: "Date",
            axisLabelUseCanvas: true,
            axisLabelFontSizePixels: 12,
            axisLabelFontFamily: 'Arial',
            axisLabelPadding: 10,
            color: "#d5d5d5"
        },
        yaxes: [{
            position: "left",
            max: 1070,
            color: "#d5d5d5",
            axisLabelUseCanvas: true,
            axisLabelFontSizePixels: 12,
            axisLabelFontFamily: 'Arial',
            axisLabelPadding: 3
        }, {
            position: "right",
            clolor: "#d5d5d5",
            axisLabelUseCanvas: true,
            axisLabelFontSizePixels: 12,
            axisLabelFontFamily: ' Arial',
            axisLabelPadding: 67
        }
        ],
        legend: {
            noColumns: 1,
            labelBoxBorderColor: "#000000",
            position: "nw"
        },
        grid: {
            hoverable: false,
            borderWidth: 0
        }
    }

    function gd(year, month, day) {
        return new Date(year, month - 1, day).getTime()
    }

    $.plot($("#flot-dashboard-chart-2"), dataset, options)
    $.plot($("#flot-dashboard-chart-3"), dataset, options)
    $.plot($("#flot-dashboard-chart-4"), dataset, options)
}

function drawPieChart(searchQuery) {
    var data = _.map(StatsSocialProp.find(searchQuery).fetch(), (d) => {
        return [
            d.type,
            d.count
        ]
    })

    c3.generate({
        bindto: '#pie',
        data: {
            columns: data,
            colors: {},
            type: 'pie'
        }
    })
}


Template.Feed.rendered = function () {
}


function getPostsForFeed(feed) {
    var posts = _.map(
        Posts.find({type: "Online", source: feed}, {limit: 100}).fetch(),
        (d) => {
            if (_.isUndefined(d.username) && !_.isUndefined(d.author))
                d.username = d.author

            d.avatar = faker.image.avatar()
            return d
        })

    return posts
}