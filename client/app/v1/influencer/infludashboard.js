var faker = require('faker')
var cache = new ReactiveDict('cache_influencer')

Template.InfluDashboard.rendered = function () {
    cache.set('influencerPosts', [])
    cache.set('selectedPost', null)
    cache.set('selectedInfluencer', null)

    drawFlotChart2()
    initDataTable()
}

function drawFlotChart2() {
    // FLOTCHART 2
    // Options, data for charts
    var data2 = [
        [gd(2012, 1, 1), 7], [gd(2012, 1, 2), 6], [gd(2012, 1, 3), 4], [gd(2012, 1, 4), 8],
        [gd(2012, 1, 5), 9], [gd(2012, 1, 6), 7], [gd(2012, 1, 7), 5], [gd(2012, 1, 8), 4],
        [gd(2012, 1, 9), 7], [gd(2012, 1, 10), 8], [gd(2012, 1, 11), 9], [gd(2012, 1, 12), 6],
        [gd(2012, 1, 13), 4], [gd(2012, 1, 14), 5], [gd(2012, 1, 15), 11], [gd(2012, 1, 16), 8],
        [gd(2012, 1, 17), 8], [gd(2012, 1, 18), 11], [gd(2012, 1, 19), 11], [gd(2012, 1, 20), 6],
        [gd(2012, 1, 21), 6], [gd(2012, 1, 22), 8], [gd(2012, 1, 23), 11], [gd(2012, 1, 24), 13],
        [gd(2012, 1, 25), 7], [gd(2012, 1, 26), 9], [gd(2012, 1, 27), 9], [gd(2012, 1, 28), 8],
        [gd(2012, 1, 29), 5], [gd(2012, 1, 30), 8], [gd(2012, 1, 31), 25]
    ]

    var data3 = [
        [gd(2012, 1, 1), 800], [gd(2012, 1, 2), 500], [gd(2012, 1, 3), 600], [gd(2012, 1, 4), 700],
        [gd(2012, 1, 5), 500], [gd(2012, 1, 6), 456], [gd(2012, 1, 7), 800], [gd(2012, 1, 8), 589],
        [gd(2012, 1, 9), 467], [gd(2012, 1, 10), 876], [gd(2012, 1, 11), 689], [gd(2012, 1, 12), 700],
        [gd(2012, 1, 13), 500], [gd(2012, 1, 14), 600], [gd(2012, 1, 15), 700], [gd(2012, 1, 16), 786],
        [gd(2012, 1, 17), 345], [gd(2012, 1, 18), 888], [gd(2012, 1, 19), 888], [gd(2012, 1, 20), 888],
        [gd(2012, 1, 21), 987], [gd(2012, 1, 22), 444], [gd(2012, 1, 23), 999], [gd(2012, 1, 24), 567],
        [gd(2012, 1, 25), 786], [gd(2012, 1, 26), 666], [gd(2012, 1, 27), 888], [gd(2012, 1, 28), 900],
        [gd(2012, 1, 29), 178], [gd(2012, 1, 30), 555], [gd(2012, 1, 31), 993]
    ]

    var dataset = [
        {
            label: "Actual Figures",
            data: data3,
            color: "#1ab394",
            bars: {
                show: true,
                align: "center",
                barWidth: 24 * 60 * 60 * 600,
                lineWidth: 0
            }

        }, {
            label: "Average",
            data: data2,
            yaxis: 2,
            color: "#1C84C6",
            lines: {
                lineWidth: 1,
                show: true,
                fill: true,
                fillColor: {
                    colors: [{
                        opacity: 0.2
                    }, {
                        opacity: 0.2
                    }]
                }
            },
            splines: {
                show: false,
                tension: 0.6,
                lineWidth: 1,
                fill: 0.1
            }
        }
    ]


    var options = {
        xaxis: {
            mode: "time",
            tickSize: [3, "day"],
            tickLength: 0,
            axisLabel: "Date",
            axisLabelUseCanvas: true,
            axisLabelFontSizePixels: 12,
            axisLabelFontFamily: 'Arial',
            axisLabelPadding: 10,
            color: "#d5d5d5"
        },
        yaxes: [{
            position: "left",
            max: 1070,
            color: "#d5d5d5",
            axisLabelUseCanvas: true,
            axisLabelFontSizePixels: 12,
            axisLabelFontFamily: 'Arial',
            axisLabelPadding: 3
        }, {
            position: "right",
            clolor: "#d5d5d5",
            axisLabelUseCanvas: true,
            axisLabelFontSizePixels: 12,
            axisLabelFontFamily: ' Arial',
            axisLabelPadding: 67
        }
        ],
        legend: {
            noColumns: 1,
            labelBoxBorderColor: "#000000",
            position: "nw"
        },
        grid: {
            hoverable: false,
            borderWidth: 0
        }
    }

    function gd(year, month, day) {
        return new Date(year, month - 1, day).getTime()
    }

    $.plot($("#flot-dashboard-chart-2"), dataset, options)
    $.plot($("#flot-dashboard-chart-3"), dataset, options)
    $.plot($("#flot-dashboard-chart-4"), dataset, options)
}

function initDataTable() {
    // Initialize dataTables
    $('.dataTables-container').DataTable({
        dom: '<"html5buttons"B>lTfgitp',
        buttons: [
            {extend: 'copy'},
            {extend: 'csv'},
            {extend: 'excel', title: 'influencer_export'},
            {extend: 'pdf', title: 'influencer_export'},

            {
                extend: 'print',
                customize: function (win) {
                    $(win.document.body).addClass('white-bg');
                    $(win.document.body).css('font-size', '10px');

                    $(win.document.body).find('table')
                        .addClass('compact')
                        .css('font-size', 'inherit');
                }
            }
        ]
    })
}

Template.InfluDashboard.helpers({
    influencerPosts: () => {
        return cache.get('influencerPosts')
    },

    influencersList: () => {
        setTimeout(() => {
            initDataTable() // fallback incase table is not initialized
        }, 500)
        return generateFakeListOfInfluencers()
    },
    ifNotEmptyPosts: () => {
        return !_.isNull(cache.get('influencerPosts'))
    },

    ifReadMode: () => {
        return ifReadMode()
    },

    selectedInfluencerPost: () => {
        return cache.get('selectedPost')
    }
})

function ifReadMode() {
    return !_.isNull(cache.get('selectedPost'))
}

function generateFakeListOfInfluencers() {
    var sources = ["twitter", "linkedin", "facebook", "france24", "bbc"]
    return _.map(_.range(200), (i) => {
        var username = faker.internet.userName()
        return {
            _id: username,
            userHandle: username,
            firstName: faker.name.firstName(),
            lastName: faker.name.lastName(),
            email: faker.internet.email(),
            followers: _.random(100, 10000),
            following: _.random(100, 10000),
            reach: _.random(1000, 100000),
            source: sources[_.random(0, sources.length - 1)]
        }
    })
}

Template.InfluDashboard.events({
    'click .influencer-container': function (event) {
        cache.set('selectedPost', null)  // turning off read mode

        var $this = $(event.target)
        cache.set('selectedInfluencer', this)
        refreshInfluencerPosts(false)
    },

    'click .influencer-post': function () {
        cache.set('selectedPost', this)
        refreshInfluencerPosts(true)
    }
})

function refreshInfluencerPosts(ifResize) {
    var influencerPosts
    if(!ifResize) {
        influencerPosts = generateFakePosts(cache.get('selectedInfluencer'))
    } else {
        influencerPosts =  _.map(cache.get('influencerPosts'), (d) => {
            d.class = (ifReadMode() ? "col-sm-12" : "col-sm-4")
            return d
        })
    }

    cache.set('influencerPosts', influencerPosts)
}

function generateFakePosts(user) {
    return _.map(_.range(20), (d) => {
        return {
            title: faker.lorem.sentence(),
            author: user._id,
            body: faker.lorem.sentences(_.random(30, 100)),
            date: faker.date.recent(),
            class: (ifReadMode() ? "col-sm-12" : "col-sm-4"),
            height: (ifReadMode() ? "auto" : "250px"),
        }
    })
}