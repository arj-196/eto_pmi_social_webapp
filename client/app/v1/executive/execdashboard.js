Template.ExecutiveSummaryDashboard.helpers({})


Template.ExecutiveSummaryDashboard.rendered = function () {
    drawDoughnutCharts()

    drawFlotChart1()

    drawFlotChart2()


    // VECTOR MAP
    var i = 0
    var showTimeLeftForVectorMap = function () {
        var subHeader = $('#vector-map-subheader')
        i++
        if (i > 20) {
            clearInterval(t)
            subHeader.text('')
        } else {
            subHeader.text('Loading in ' + (20 - i) + ' seconds')
        }

    }
    var t = setInterval(showTimeLeftForVectorMap, 1000);

    setTimeout(() => {
        // drawVectorMap()
    }, 20000)
}

function drawDoughnutCharts() {
    //DOUGHNUT CHART
    // Options, data for doughnut chart
    var doughnutOptions = {
        segmentShowStroke: true,
        segmentStrokeColor: "#fff",
        segmentStrokeWidth: 2,
        percentageInnerCutout: 45, // This is 0 for Pie charts
        animationSteps: 100,
        animationEasing: "easeOutBounce",
        animateRotate: true,
        animateScale: false
    }
    var doughnutData

    // DOUGHNUT 1
    doughnutData = [
        {
            value: 60,
            color: "#a3e1d4",
            highlight: "#1ab394",
            label: "twitter"
        },
        {
            value: 30,
            color: "#dedede",
            highlight: "#1ab394",
            label: "facebook"
        },
        {
            value: 10,
            color: "#A4CEE8",
            highlight: "#A4CEE8",
            label: "linkedIn"
        }
    ]

    new Chart(document.getElementById("doughnutChart1").getContext("2d")).Doughnut(doughnutData, doughnutOptions)

    // DOUGHNUT 2
    doughnutData = [
        {
            value: 35,
            color: "#a3e1d4",
            highlight: "#1ab394",
            label: "DOCTISSIMO.FR"
        },
        {
            value: 20,
            color: "#dedede",
            highlight: "#1ab394",
            label: "HOMECINEMA-FR"
        },
        {
            value: 5,
            color: "#A4CEE8",
            highlight: "#A4CEE8",
            label: "FORUM AUTO"
        },
        {
            value: 40,
            color: "#f49242",
            highlight: "#ce7f3d",
            label: "FORUMS FRANCE 2"
        }
    ]
    new Chart(document.getElementById("doughnutChart2").getContext("2d")).Doughnut(doughnutData, doughnutOptions)

    // DOUGHNUT 3
    doughnutData = [
        {
            value: 60,
            color: "#a3e1d4",
            highlight: "#1ab394",
            label: "press"
        },
        {
            value: 30,
            color: "#dedede",
            highlight: "#1ab394",
            label: "radio"
        },
        {
            value: 10,
            color: "#A4CEE8",
            highlight: "#A4CEE8",
            label: "tv"
        }
    ]
    new Chart(document.getElementById("doughnutChart3").getContext("2d")).Doughnut(doughnutData, doughnutOptions)
}

function drawFlotChart1() {
    // FLOTCHART 1
    // Options/data for flot chart
    var data1 = _.map(_.range(31), function (d) {
        var y
        if (d < 20)
            y = _.random(10, 200) + 50
        else
            y = null
        return [d, y]
    })
    var data2 = _.map(_.range(31), function (d) {
        return [d, _.random(10, 200)]
    })

    var floatChartContainer = $("#flot-dashboard-chart-1")

    floatChartContainer.length &&
    $.plot(floatChartContainer,
        // [data1, data2]
        [
            {label: "Sep 2016", data: data1},
            {label: "Aug 2016", data: data2},
        ]
        ,
        {
            series: {
                lines: {
                    show: false,
                    fill: true
                },
                splines: {
                    show: true,
                    tension: 0.4,
                    lineWidth: 1,
                    fill: 0.4
                },
                points: {
                    radius: 0,
                    show: true
                },
                shadowSize: 2
            },
            grid: {
                hoverable: true,
                clickable: true,
                tickColor: "#d5d5d5",
                borderWidth: 1,
                color: '#d5d5d5'
            },
            colors: ["#1ab394", "#1C84C6"],
            xaxis: {},
            yaxis: {
                ticks: 4
            },
            tooltip: true,
            legend: {
                show: true,
                backgroundOpacity: 0.5,
                backgroundColor: "#0e190e",
                margin: [2, 2]
            },
        }
    )
}

function drawFlotChart2() {
    // FLOTCHART 2
    // Options, data for charts
    var data2 = [
        [gd(2012, 1, 1), 7], [gd(2012, 1, 2), 6], [gd(2012, 1, 3), 4], [gd(2012, 1, 4), 8],
        [gd(2012, 1, 5), 9], [gd(2012, 1, 6), 7], [gd(2012, 1, 7), 5], [gd(2012, 1, 8), 4],
        [gd(2012, 1, 9), 7], [gd(2012, 1, 10), 8], [gd(2012, 1, 11), 9], [gd(2012, 1, 12), 6],
        [gd(2012, 1, 13), 4], [gd(2012, 1, 14), 5], [gd(2012, 1, 15), 11], [gd(2012, 1, 16), 8],
        [gd(2012, 1, 17), 8], [gd(2012, 1, 18), 11], [gd(2012, 1, 19), 11], [gd(2012, 1, 20), 6],
        [gd(2012, 1, 21), 6], [gd(2012, 1, 22), 8], [gd(2012, 1, 23), 11], [gd(2012, 1, 24), 13],
        [gd(2012, 1, 25), 7], [gd(2012, 1, 26), 9], [gd(2012, 1, 27), 9], [gd(2012, 1, 28), 8],
        [gd(2012, 1, 29), 5], [gd(2012, 1, 30), 8], [gd(2012, 1, 31), 25]
    ]

    var data3 = [
        [gd(2012, 1, 1), 800], [gd(2012, 1, 2), 500], [gd(2012, 1, 3), 600], [gd(2012, 1, 4), 700],
        [gd(2012, 1, 5), 500], [gd(2012, 1, 6), 456], [gd(2012, 1, 7), 800], [gd(2012, 1, 8), 589],
        [gd(2012, 1, 9), 467], [gd(2012, 1, 10), 876], [gd(2012, 1, 11), 689], [gd(2012, 1, 12), 700],
        [gd(2012, 1, 13), 500], [gd(2012, 1, 14), 600], [gd(2012, 1, 15), 700], [gd(2012, 1, 16), 786],
        [gd(2012, 1, 17), 345], [gd(2012, 1, 18), 888], [gd(2012, 1, 19), 888], [gd(2012, 1, 20), 888],
        [gd(2012, 1, 21), 987], [gd(2012, 1, 22), 444], [gd(2012, 1, 23), 999], [gd(2012, 1, 24), 567],
        [gd(2012, 1, 25), 786], [gd(2012, 1, 26), 666], [gd(2012, 1, 27), 888], [gd(2012, 1, 28), 900],
        [gd(2012, 1, 29), 178], [gd(2012, 1, 30), 555], [gd(2012, 1, 31), 993]
    ]

    var dataset = [
        {
            label: "Actual Figures",
            data: data3,
            color: "#1ab394",
            bars: {
                show: true,
                align: "center",
                barWidth: 24 * 60 * 60 * 600,
                lineWidth: 0
            }

        }, {
            label: "Average",
            data: data2,
            yaxis: 2,
            color: "#1C84C6",
            lines: {
                lineWidth: 1,
                show: true,
                fill: true,
                fillColor: {
                    colors: [{
                        opacity: 0.2
                    }, {
                        opacity: 0.2
                    }]
                }
            },
            splines: {
                show: false,
                tension: 0.6,
                lineWidth: 1,
                fill: 0.1
            }
        }
    ]


    var options = {
        xaxis: {
            mode: "time",
            tickSize: [3, "day"],
            tickLength: 0,
            axisLabel: "Date",
            axisLabelUseCanvas: true,
            axisLabelFontSizePixels: 12,
            axisLabelFontFamily: 'Arial',
            axisLabelPadding: 10,
            color: "#d5d5d5"
        },
        yaxes: [{
            position: "left",
            max: 1070,
            color: "#d5d5d5",
            axisLabelUseCanvas: true,
            axisLabelFontSizePixels: 12,
            axisLabelFontFamily: 'Arial',
            axisLabelPadding: 3
        }, {
            position: "right",
            clolor: "#d5d5d5",
            axisLabelUseCanvas: true,
            axisLabelFontSizePixels: 12,
            axisLabelFontFamily: ' Arial',
            axisLabelPadding: 67
        }
        ],
        legend: {
            noColumns: 1,
            labelBoxBorderColor: "#000000",
            position: "nw"
        },
        grid: {
            hoverable: false,
            borderWidth: 0
        }
    }

    function gd(year, month, day) {
        return new Date(year, month - 1, day).getTime()
    }

    $.plot($("#flot-dashboard-chart-2"), dataset, options)
    $.plot($("#flot-dashboard-chart-3"), dataset, options)
    $.plot($("#flot-dashboard-chart-4"), dataset, options)
}

function drawVectorMap() {  // waiting for db to load
    var volumes = LastVolumes.find({})
    if (volumes.count() != 21) return
    var gdpData = {}
    volumes.forEach(function (volume) {
        gdpData[volume.country.toLowerCase()] = {
            ratio_V_Vmax: volume.ratio_V_Vmax,
            phase: volume.phase,
            avancmt_phase: volume.avancmt_phase,
            volume: volume.volume,
            date: volume.date,
            P1: Math.round(volume.P1),
            P2: Math.round(volume.P2),
            P3: Math.round(volume.P3),
            P4: Math.round(volume.P4),
            object_vol: volume.object_vol
        }
    })

    var max = 0,
        min = Number.MAX_VALUE,
        cc,
        hex
    colors = {}
    for (cc in gdpData) {
        if (gdpData[cc].avancmt_phase > 0) {
            colors[cc] = '#'
            for (var i = 0; i < 3; i++) {
                hex = Math.min(255, Math.round(PhasesStartColors[gdpData[cc].phase][i]
                    + (PhasesEndColors[gdpData[cc].phase][i]
                    - PhasesStartColors[gdpData[cc].phase][i])
                    * parseFloat(gdpData[cc].avancmt_phase))).toString(16)

                if (hex.length == 1) {
                    hex = '0' + hex
                }

                colors[cc] += (hex.length == 1 ? '0' : '') + hex
            }
        }
    }
    $("#vmap_changecontrol").vectorMap({
        map: 'world_en',
        backgroundColor: null,
        color: '#999999',
        hoverColor: '#1ABC9C',
        selectedColor: '#0a846c',
        enableZoom: true,
        showTooltip: true,
        colors: colors,
        onResize: function (element, width, height) {
        },
        onRegionClick: function (element, code, region) {
            if ($.inArray(code.toUpperCase(), CountryCodes) != -1) {
                $('#strategicchangecontrol-map-container').removeClass("col-lg-12").addClass("col-lg-9")
                $('#strategicchangecontrol-detail-container').show(300)

                $('#selected-country-name').html(region)
                $('#selected-country-volume').html(numberWithCommas(Math.round(gdpData[code].volume)))
                $('#selected-country-phase').html(PrettyPhase[gdpData[code].phase])
                $('#selected-country-phase').css("background-color", PhasesStartColors[gdpData[code].phase])

                //Platform Donut
                // Options, data for doughnut chart
                var doughnutData = []
                for (var i = 0; i < Platforms.length; i++) {
                    doughnutData.push({
                        value: gdpData[code][Platforms[i].code],
                        color: PlatformColors[Platforms[i].code],
                        label: Platforms[i].name
                    })
                }
                var doughnutOptions = {
                    segmentShowStroke: true,
                    segmentStrokeColor: "#fff",
                    segmentStrokeWidth: 2,
                    percentageInnerCutout: 40, // This is 0 for Pie charts
                    animationSteps: 40,
                    animationEasing: "easeOutExpo",
                    animateRotate: true,
                    animateScale: false
                }
                var ctx = document.getElementById("strategicchangecontrol-platform-donut").getContext("2d")
                if (typeof PenetrationChart === 'undefined') {
                    PlatformChart = new Chart(ctx)
                    var tmp = PlatformChart.Doughnut(doughnutData, doughnutOptions)
                    document.getElementById('js-legend').innerHTML = tmp.generateLegend()

                } else {
                    PlatformChart.Doughnut(doughnutData, doughnutOptions)
                }

                //objective
                var obj = Math.round(100 * (gdpData[code].volume / gdpData[code].object_vol))

                $("#selected-country-objective").html(obj + '% <i class="fa fa-bolt"></i>')

                if (obj >= 100) {
                    $("#selected-country-objective").removeClass('text-danger')
                    $("#selected-country-objective").addClass('text-navy')
                } else {
                    $("#selected-country-objective").addClass('text-danger')
                    $("#selected-country-objective").removeClass('text-navy')
                }
                //Penetration

                if (typeof PenetrationChart === 'undefined') {

                    PenetrationChart = c3.generate({
                        bindto: '#strategicchangecontrol-penetration-gauge',
                        data: {
                            columns: [
                                ['data', 100 * gdpData[code].ratio_V_Vmax]
                            ],
                            type: 'gauge'
                        },
                        color: function (color, d) {
                            return {
                                pattern: [colors[code]], // the three color levels for the percentage values.
                                threshold: {
                                    values: [100]
                                }
                            }
                        }

                    })
                } else {
                    PenetrationChart.load({
                        columns: [
                            ['data', 100 * gdpData[code].ratio_V_Vmax]
                        ]
                    })

                }
            } else {
                $('#strategicchangecontrol-map-container').removeClass("col-lg-9").addClass("col-lg-12")
                $('#strategicchangecontrol-detail-container').hide()
            }
        }

    })
}