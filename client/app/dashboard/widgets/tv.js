import {drawFlotChart} from '../../utils/graph/flotchart'
import {drawStackedBarChart} from '../../utils/graph/barstacked'
var cache = new ReactiveDict('tv')

Template.TVMediaWidget.rendered = function () {
    cache.set('postList', [])

    Meteor.call('graph.getData', 'gl5', [], (err, data) => {
        var floatChartContainer = $("#flot-dashboard-chart-5")
        drawFlotChart(floatChartContainer, data, "Total Number of posts")
    })

    Meteor.call('media.getPosts', 'tv', [], (err, posts) => {
        posts = _.map(posts, (d) => {
            d.sentiment.polarity = d.sentiment.polarity.toFixed(2)
            d.date = new Date(d.date)
            return d
        })
        cache.set('postList', posts)
    })

    Meteor.call('graph.getData', 'gb5', [], (err, data) => {
        drawStackedBarChart('#ct-chart7', data[0])
    })

}

Template.TVMediaWidget.helpers({
    getPosts: () => {
        return cache.get('postList')
    }
})

