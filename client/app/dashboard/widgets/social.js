var faker = require('faker')
var cache = new ReactiveDict('cache_social_media')
import {drawFlotChart} from '../../utils/graph/flotchart'
import {drawStackedBarChart} from '../../utils/graph/barstacked'


Template.SocialMediaWidget.rendered = function () {
    Meteor.call('graph.getData', 'gl2', [], (err, data) => {
        var floatChartContainer = $("#flot-dashboard-chart-2")
        drawFlotChart(floatChartContainer, data, "Total Number of posts")
    })

    Meteor.call('graph.getData', 'gb2', [], (err, data) => {
        drawStackedBarChart('#ct-chart4', data[0])
    })

    setTimeout(() => {
        cache.set('feedPosts', getPostsForFeed('twitter'))
    }, 1500)
}

Template.SocialMediaWidget.helpers({
    ifExistsPosts: () => {
        return !_.isNull(cache.get('feedPosts'))
    },

    feedPosts: () => {
        var posts = cache.get('feedPosts')
        return posts
    }
})

Template.Feed.events({
    'click .feed-trigger': function (event) {
        $('.feed-trigger').find('.list-group-item').removeClass('active')
        $('.feed-trigger[data-value="' + this.value + '"]')
            .find('.list-group-item').addClass('active')

        // refresh wordcloud
        drawWordCloud({"value.source": this.value})

        cache.set('feedPosts', getPostsForFeed(this.value))
    }
})

function getPostsForFeed(feed) {

    var posts = _.map(
        Posts.find({type: "Online", source: feed}, {limit: 100}).fetch(),
        (d) => {
            if (_.isUndefined(d.username) && !_.isUndefined(d.author))
                d.username = d.author

            d.avatar = faker.image.avatar()
            return d
        })

    return posts
}