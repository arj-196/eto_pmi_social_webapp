import {drawFlotChart} from '../../utils/graph/flotchart'
import {drawStackedBarChart} from '../../utils/graph/barstacked'
var cache = new ReactiveDict('radio')

Template.RadioMediaWidget.rendered = function () {
    cache.set('postList', [])

    Meteor.call('graph.getData', 'gl4', [], (err, data) => {
        var floatChartContainer = $("#flot-dashboard-chart-4")
        drawFlotChart(floatChartContainer, data, "Total Number of posts")
    })

    Meteor.call('media.getPosts', 'radio', [], (err, posts) => {
        posts = _.map(posts, (d) => {
            d.sentiment.polarity = d.sentiment.polarity.toFixed(2)
            d.date = new Date(d.date)
            return d
        })
        cache.set('postList', posts)
    })

    Meteor.call('graph.getData', 'gb4', [], (err, data) => {
        drawStackedBarChart('#ct-chart6', data[0])
    })

}

Template.RadioMediaWidget.helpers({
    getPosts: () => {
        return cache.get('postList')
    }
})


