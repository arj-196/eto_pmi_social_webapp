import {drawFlotChart} from '../../utils/graph/flotchart'
import {drawStackedBarChart} from '../../utils/graph/barstacked'
var cache = new ReactiveDict('press')

Template.PressMediaWidget.rendered = function () {
    cache.set('postList', [])

    Meteor.call('graph.getData', 'gl3', [], (err, data) => {
        var floatChartContainer = $("#flot-dashboard-chart-3")
        drawFlotChart(floatChartContainer, data, "Total Number of posts")
    })

    Meteor.call('graph.getData', 'gb3', [], (err, data) => {
        drawStackedBarChart('#ct-chart5', data[0])
    })

    Meteor.call('media.getPosts', 'news', [], (err, posts) => {
        posts = _.map(posts, (d) => {
            d.sentiment.polarity = d.sentiment.polarity.toFixed(2)
            d.date = new Date(d.date)
            return d
        })
        cache.set('postList', posts)
    })
}

Template.PressMediaWidget.helpers({
    getPosts: () => {
        return cache.get('postList')
    }
})

