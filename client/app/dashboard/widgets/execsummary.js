import {drawFlotChart} from '../../utils/graph/flotchart'
import {drawDoughnutChart} from '../../utils/graph/doughnut'
import {drawStackedBarChart} from '../../utils/graph/barstacked'
var cache = new ReactiveDict('execwidget')

Template.ExecutiveSummaryWidget.rendered = function () {
    cache.set('sentStat', {})

    Meteor.call('graph.getData', 'gl1', [], (err, data) => {
        var floatChartContainer = $("#flot-dashboard-chart-1")
        drawFlotChart(floatChartContainer, data, "Total Number of posts")
    })

    Meteor.call('graph.getData', 'gl1', [], (err, data) => {
        var floatChartContainer = $("#flot-dashboard-chart-1")
        drawFlotChart(floatChartContainer, data, "Total Number of posts")
    })

    // Doughnut charts
    Meteor.call('graph.getData', 'gp2', [], (err, data) => {
        drawDoughnutChart('doughnutChart1', data)
    })
    Meteor.call('graph.getData', 'gp3', [], (err, data) => {
        drawDoughnutChart('doughnutChart2', data)
    })
    Meteor.call('graph.getData', 'gp4', [], (err, data) => {
        drawDoughnutChart('doughnutChart3', data)
    })
    Meteor.call('graph.getData', 'gp5', [], (err, data) => {
        drawDoughnutChart('doughnutChart4', data)
    })

    Meteor.call('graph.getData', 'gb1', [], (err, data) => {
        drawStackedBarChart('#ct-chart3', data[0])
    })

    Meteor.call('graph.getData', 'c1', [], (err, data) => {
        console.log(data)
        cache.set('sentStat', data[0])
    })
}

Template.ExecutiveSummaryWidget.helpers({
    sentStat: () => {
        return cache.get('sentStat')
    }
})

