OperationalCountries = ['DE', 'GB', 'FR'];

Segments = [
    {
        name: "Legal Age Smokers",
        image: "/img/smoker.jpeg",
        code: "LAS"
    },
    {
        name: "Non-Smokers",
        image: "/img/non-smoker.png",
        code: "NS"
    },
    {
        name: "Scientists",
        image: "/img/scientist.jpeg",
        code: "SC"
    },
    {
        name: "Doctors",
        image: "/img/doctor.jpeg",
        code: "DR"
    },
    {
        name: "Journalists",
        image: "/img/journalist.jpeg",
        code: "JR"
    },
    {
        name: "Politicians",
        image: "/img/politician.jpeg",
        code: "PL"
    },
    {
        name: "Retailers",
        image: "/img/retailer.jpeg",
        code: "RT"
    }
]

Phases = ["innovators", "early_adopters", "early_majority", "late_majority", "lagards"];
PrettyPhase = {
    innovators: 'Innovators',
    early_adopters: 'Early Adopters',
    early_majority: 'Early Majority',
    late_majority: 'Late Majority',
    lagards: 'Lagards',
}

PhasesStartColors = {
    innovators: [11, 86, 161],
    early_adopters: [69, 141, 189],
    early_majority: [174, 202, 220],
    late_majority: [237, 182, 151],
    lagards: [219, 84, 35]
}

PhasesEndColors = {
    innovators: [69, 141, 189],
    early_adopters: [174, 202, 220],
    early_majority: [237, 182, 151],
    late_majority: [219, 84, 35],
    lagards: [167, 5, 18]
}


Platforms = [
    {
        code: 'P1',
        name: 'Platform 1',
        image: '/img/P1.png'
    },
    {
        code: 'P2',
        name: 'Platform 2',
        image: '/img/P2.png'
    },
    {
        code: 'P3',
        name: 'Platform 3',
        image: '/img/P3.png'
    },
    {
        code: 'P4',
        name: 'Platform 4',
        image: '/img/P4.png'
    }
]

PlatformColors = {
    'P1': '#3498DB',
    'P2': '#F1C40F',
    'P3': '#E74C3C',
    'P4': '#1ABC9C'
};

CountryCodes = ['IT', 'FR', 'DE', 'GB', 'ES', 'PT', 'RO', 'AU', 'HK', 'JP', 'ZA', 'MA', 'RU', 'UA', 'AE', 'IL', 'BR', 'CO', 'CA', 'DK', 'KR'];
Countries = {
    'IT': 'Italy',
    'FR': 'France',
    'DE': 'Germany',
    'GB': 'United Kingdom',
    'ES': 'Spain',
    'PT': 'Portugal',
    'RO': 'Australia',
    'AU': 'Romania',
    'HK': 'Hong Kong',
    'JP': 'Japan',
    'ZA': 'South Africa',
    'MA': 'Morocco',
    'RU': 'Russia',
    'UA': 'Ukraine',
    'AE': 'United Arab Emirates',
    'IL': 'Israel',
    'BR': 'Brazil',
    'CO': 'Colombia',
    'CA': 'Canada',
    'DK': 'Denmark',
    'KR': 'Korea'
}

// Events = new Mongo.Collection("events");
// Quote = new Mongo.Collection("quote");
// Reputation = new Mongo.Collection("reputation");
// Concept = new Mongo.Collection("concept");
// Resume = new Mongo.Collection("resumes");
// LastVolumes = new Mongo.Collection("last_volume");
// LastReputation = new Mongo.Collection("last_reputation");
// LastConceptAcceptance = new Mongo.Collection("last_concept");
// LastTopOfMind = new Mongo.Collection("last_topofmind");
// LastSentiment = new Mongo.Collection("last_sentiment");
// LastVolumesCountry = new Mongo.Collection("last_volumes_country");
// VolumesGlobal = new Mongo.Collection("volume_global");
// LastVolumesGlobal = new Mongo.Collection("last_volume_global");
// Penetration = new Mongo.Collection("changecontrol_volume");
// Switch = new Mongo.Collection("changecontrol_switch");
// EventsAction = new Mongo.Collection("events_action");

Posts = new Mongo.Collection("posts")
StatsTopic = new Mongo.Collection("stats_topic")
StatsTopicProp = new Mongo.Collection("stats_topic_prop")
StatsSocialProp = new Mongo.Collection("stats_social_prop")
StatsTopicWordCloud = new Mongo.Collection("stats_topic_wordcloud")
StatsSocialWordCloud = new Mongo.Collection("stats_social_wordcloud")
Graphs = new Mongo.Collection('test_graph')
CleanedPosts = new Mongo.Collection('posts_cleaned')

numberWithCommas = function (x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}
